#include "String_helper.h"

#include <algorithm>

std::vector<std::string> StringHelper::split(std::string str, std::string delimiter)
{
    std::vector<std::string> ret;
    size_t pos = 0;
    std::string token;
    while ((pos = str.find(delimiter)) != std::string::npos) {
        ret.push_back(str.substr(0, pos));
        str.erase(0, pos + delimiter.length());
    }
    ret.push_back(str.substr(0, pos));
    return ret;
}

void StringHelper::ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

void StringHelper::rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

void StringHelper::vtrim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

std::string StringHelper::ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

std::string StringHelper::rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

std::string StringHelper::trim_copy(std::string s) {
    vtrim(s);
    return s;
}
// ------ new string mehtods --------------------------------- 
std::vector<std::string> StringHelper::split(std::string str, const char ch)
{
    std::vector<std::string> ret;
    size_t pos = 0;
    std::string token;
    while ((pos = str.find(ch)) != std::string::npos) {
        ret.push_back(str.substr(0, pos));
        str.erase(0, pos + 1); //delimiter.length());
    }
    ret.push_back(str.substr(0, pos));
    return ret;
}

std::string StringHelper::trim(std::string txt, const char ch)
{
    std::string res;
    char prev;
    size_t txtsize = txt.size();
    for (size_t i = 0; i < txtsize; i++) {
        if(i == 0) {
            if(txt[i] == ch) continue;
        }
        else {
            prev = txt[i-1];
            if ((prev == ch && txt[i] == ch) || (i == txt.size()-1 && txt[i] == ch)) continue;
        }
        res.push_back(txt[i]);
    }
    return res;
}

std::string StringHelper::join(std::vector<std::string> vec, const char ch)
{
    std::string res;
    size_t vecsize = vec.size();
    for (size_t i = 0; i < vecsize; i++) {
        res.append(vec[i]);
        if (i != vecsize - 1) res.push_back(ch);
    }
    return res;
}

bool StringHelper::replace(std::string &str, const std::string &from, const std::string &to)
{
    size_t start_pos = str.find(from);
    if (start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

bool StringHelper::replace_all(std::string &str, const std::vector<std::string> &old, const std::vector<std::string> &nw)
{
    size_t oldsize = old.size();
    size_t nwsize = nw.size();
    if (nwsize == 1) {
        for(size_t i = 0; i < oldsize; i++) {
            replace(str, old[i], nw[0]);
        }
        return true;
    }
    else {
        if(oldsize != nwsize)
            return false;
        for(size_t i = 0; i < oldsize; i++) {
            replace(str, old[i], nw[i]);
        }
        return true;
    }
}

bool StringHelper::is_numeric(std::string str)
{
    return !str.empty() &&
            std::find_if(str.begin(), str.end(), [](unsigned char c) { return !std::isdigit(c); }) == str.end();
}
