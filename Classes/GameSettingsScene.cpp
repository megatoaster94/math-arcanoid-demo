/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "GameSettingsScene.h"
#include "GameScene.h"
#include "String_helper.h"

#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/prettywriter.h"
#include "json/writer.h"

#include <fstream>

USING_NS_CC;

Scene* GameSettingsScene::createScene()
{
    return GameSettingsScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameSettingsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto board_size_label = Label::createWithSystemFont("Board size", "Arial", 54);
    board_size_label->setPosition(200, visibleSize.height - 70);
    this->addChild(board_size_label);

    auto board_size_width = Label::createWithSystemFont(std::to_string((int)_board_size.width), "Arial", 54);
    board_size_width->setPosition(550, visibleSize.height - 70);
    this->addChild(board_size_width);

    auto board_size_height = Label::createWithSystemFont(std::to_string((int)_board_size.height), "Arial", 54);
    board_size_height->setPosition(650, visibleSize.height - 70);
    this->addChild(board_size_height);

    auto board_size_width_plus = MenuItemFont::create("+", [this, board_size_width](Ref* sender) {
            _board_size.width += 1;
            _board_size.width = std::min((int)_board_size.width, _max_board_size_width);
            board_size_width->setString(std::to_string((int)_board_size.width));
        });
    board_size_width_plus->setPosition(Vec2(550, visibleSize.height - 30));

    auto board_size_width_minus = MenuItemFont::create("-", [this, board_size_width](Ref* sender) {
            _board_size.width -= 1;
            _board_size.width = std::max((int)_board_size.width, _min_board_size_width);
            board_size_width->setString(std::to_string((int)_board_size.width));
        });
    board_size_width_minus->setPosition(Vec2(550, visibleSize.height - 110));

    auto board_size_height_plus = MenuItemFont::create("+", [this, board_size_height](Ref* sender) {
            _board_size.height += 1;
            _board_size.height = std::min((int)_board_size.height, _max_board_size_height);
            board_size_height->setString(std::to_string((int)_board_size.height));
        });
    board_size_height_plus->setPosition(Vec2(650, visibleSize.height - 30));

    auto board_size_height_minus = MenuItemFont::create("-", [this, board_size_height](Ref* sender) {
            _board_size.height -= 1;
            _board_size.height = std::max((int)_board_size.height, _min_board_size_height);
            board_size_height->setString(std::to_string((int)_board_size.height));
        });
    board_size_height_minus->setPosition(Vec2(650, visibleSize.height - 110));



    auto platform_size_label = Label::createWithSystemFont("Platform size", "Arial", 54);
    platform_size_label->setPosition(Vec2(200, visibleSize.height - 190));
    this->addChild(platform_size_label);

    auto platform_size = Label::createWithSystemFont(std::to_string(_platform_size), "Arial", 54);
    platform_size->setPosition(650, visibleSize.height - 190);
    this->addChild(platform_size);

    auto platform_size_plus = MenuItemFont::create("+", [this, platform_size](Ref* sender) {
            _platform_size += 1;
            _platform_size = std::min(_platform_size, (unsigned int)_max_platform_size);
            platform_size->setString(std::to_string(_platform_size));
        });
    platform_size_plus->setPosition(Vec2(650, visibleSize.height - 150));

    auto platform_size_minus = MenuItemFont::create("-", [this, platform_size](Ref* sender) {
            _platform_size -= 1;
            _platform_size = std::max(_platform_size, (unsigned int)_min_platform_size);
            platform_size->setString(std::to_string(_platform_size));
        });
    platform_size_minus->setPosition(Vec2(650, visibleSize.height - 230));



    auto block_size_label = Label::createWithSystemFont("Block size", "Arial", 54);
    block_size_label->setPosition(Vec2(200, visibleSize.height - 310));
    this->addChild(block_size_label);

    auto block_size_width = Label::createWithSystemFont(std::to_string((int)_block_size.width), "Arial", 54);
    block_size_width->setPosition(550, visibleSize.height - 310);
    this->addChild(block_size_width);

    auto block_size_height = Label::createWithSystemFont(std::to_string((int)_block_size.height), "Arial", 54);
    block_size_height->setPosition(650, visibleSize.height - 310);
    this->addChild(block_size_height);

    auto block_size_width_plus = MenuItemFont::create("+", [this, block_size_width](Ref* sender) {
            _block_size.width += 1;
            _block_size.width = std::min((int)_block_size.width, _max_block_size_width);
            block_size_width->setString(std::to_string((int)_block_size.width));
        });
    block_size_width_plus->setPosition(Vec2(550, visibleSize.height - 270));

    auto block_size_width_minus = MenuItemFont::create("-", [this, block_size_width](Ref* sender) {
            _block_size.width -= 1;
            _block_size.width = std::max((int)_block_size.width, _min_block_size_width);
            block_size_width->setString(std::to_string((int)_block_size.width));
        });
    block_size_width_minus->setPosition(Vec2(550, visibleSize.height - 350));

    auto block_size_height_plus = MenuItemFont::create("+", [this, block_size_height](Ref* sender) {
            _block_size.height += 1;
            _block_size.height = std::min((int)_block_size.height, _max_block_size_height);
            block_size_height->setString(std::to_string((int)_block_size.height));
        });
    block_size_height_plus->setPosition(Vec2(650, visibleSize.height - 270));

    auto block_size_height_minus = MenuItemFont::create("-", [this, block_size_height](Ref* sender) {
            _block_size.height -= 1;
            _block_size.height = std::max((int)_block_size.height, _min_block_size_height);
            block_size_height->setString(std::to_string((int)_block_size.height));
        });
    block_size_height_minus->setPosition(Vec2(650, visibleSize.height - 350));



    auto game_speed_label = Label::createWithSystemFont("Game speed", "Arial", 54);
    game_speed_label->setPosition(Vec2(200, visibleSize.height - 430));
    this->addChild(game_speed_label);

    auto game_speed = Label::createWithSystemFont(std::to_string((int)_game_speed), "Arial", 54);
    game_speed->setPosition(650, visibleSize.height - 430);
    this->addChild(game_speed);

    auto game_speed_plus = MenuItemFont::create("+", [this, game_speed](Ref* sender) {
            _game_speed += 1;
            _game_speed = std::min((int)_game_speed, _max_game_speed);
            game_speed->setString(std::to_string((int)_game_speed));
        });
    game_speed_plus->setPosition(Vec2(650, visibleSize.height - 390));

    auto game_speed_minus = MenuItemFont::create("-", [this, game_speed](Ref* sender) {
            _game_speed -= 1;
            _game_speed = std::max((int)_game_speed, _min_game_speed);
            game_speed->setString(std::to_string((int)_game_speed));
        });
    game_speed_minus->setPosition(Vec2(650, visibleSize.height - 470));



    auto back_item = MenuItemFont::create("Back", [&](Ref* sender) {
            Director::getInstance()->popScene();
        });
    back_item->setPosition(Vec2(60, 40));

    auto play_item = MenuItemFont::create("Play", [&](Ref* sender) {
            std::queue<std::string> expressions;
            std::vector<std::vector<arcanoid::Block>> blocks_vector;

            std::ifstream file(FileUtils::getInstance()->fullPathForFilename("blocks.txt"));

            if (!file.is_open()) {
                log("Error reading file!");
                return;
            }

            std::string line;

            while (std::getline(file, line)) {
                log("%s", line.c_str());
                std::vector<std::string> splitted_line = StringHelper::split(line, ",");

                if (splitted_line.size() < 3) {
                    log("Wrong file format in line: %s", line.c_str());
                    return;
                }

                std::vector<arcanoid::Block> round_blocks;

                size_t spl_str_size = splitted_line.size();
                for (size_t i = 0; i < spl_str_size; ++i) {
                    if (splitted_line[i].empty() || splitted_line[i].find_first_not_of(" ") == std::string::npos) {
                        log("Wrong file format in line: %s", line.c_str());
                        return;
                    }

                    if (i == 0) {
                        expressions.push(splitted_line[i]);
                    }
                    else if (i == 1) {
                        round_blocks.push_back(arcanoid::Block(Point(0, 0), _block_size, splitted_line[i], true));
                    }
                    else {
                        round_blocks.push_back(arcanoid::Block(Point(0, 0), _block_size, splitted_line[i], false));
                    }
                }

                blocks_vector.push_back(round_blocks);
            }


            file.close();

            std::queue<std::queue<arcanoid::Block>> blocks_queue;

            std::random_device rd;
            auto rng = std::default_random_engine { rd() };

            for (auto rnd_blocks : blocks_vector) {
                std::shuffle(std::begin(rnd_blocks), std::end(rnd_blocks), rng);

                std::queue<arcanoid::Block> round_blocks;

                for (auto blck : rnd_blocks) {
                    round_blocks.push(blck);
                }

                blocks_queue.push(round_blocks);
            }


            auto game_scene = GameScene::createScene(_board_size, _platform_size, _block_size, _game_speed, expressions, blocks_queue);
            /*auto game_scene = GameScene::createScene(Size(20, 20),
                                                     4,
                                                     Size(2, 2),
                                                     1);*/
            //Director::getInstance()->replaceScene(TransitionSlideInR::create(1.0f, game_scene));
            Director::getInstance()->pushScene(TransitionSlideInR::create(.3f, game_scene));
        });
    play_item->setPosition(Vec2(visibleSize.width - 60, 40));

    auto load_settings_item = MenuItemFont::create("Load settings",
        //capture all by reference not working :(
        [this, board_size_width, board_size_height, platform_size, block_size_width, block_size_height, game_speed] (Ref* sender) {
            std::ifstream file(FileUtils::getInstance()->fullPathForFilename("settings.json"));

            if (!file.is_open()) {
                log("Error reading file!");
                return;
            }

            std::string json_str((std::istreambuf_iterator<char>(file)),
                                 std::istreambuf_iterator<char>());

            file.close();

            log("JSON string:\n%s", json_str.c_str());

            rapidjson::Document document;

            if (document.Parse(json_str.c_str()).HasParseError())
            {
                log("Error parsing JSON settings file: %d", document.GetParseError());
                return;
            }

            //TODO: add check if members more than necessary

            if (!document.HasMember("board_size")) {
                log("Document has no member \"board_size\"");
                return;
            }

            if (!document["board_size"].IsObject()) {
                log("board_size must be object");
                return;
            }

            auto board_size = document["board_size"].GetObject();

            if (!board_size.HasMember("width")) {
                log("board_size has no member \"width\"");
                return;
            }
            if (!board_size.HasMember("height")) {
                log("board_size has no member \"height\"");
                return;
            }

            if (!document["board_size"]["width"].IsInt()) {
                log("board_size/width wrong data type");
                return;
            }
            if (!document["board_size"]["height"].IsInt()) {
                log("board_size/height wrong data type");
                return;
            }

            int board_size_width_value = document["board_size"]["width"].GetInt();
            int board_size_height_value = document["board_size"]["height"].GetInt();
            log("Board_size: %dx%d", board_size_width_value, board_size_height_value);


            if (!document.HasMember("platform_size")) {
                log("Document has no member \"platform_size\"");
                return;
            }

            if (!document["platform_size"].IsInt()) {
                log("platform_size wrong data type");
                return;
            }

            unsigned int platform_size_value = document["platform_size"].GetInt();
            log("Platform size: %d", platform_size_value);


            if (!document.HasMember("block_size")) {
                log("Document has no member \"block_size\"");
                return;
            }

            if (!document["block_size"].IsObject()) {
                log("block_size must be object");
                return;
            }

            auto block_size = document["block_size"].GetObject();

            if (!block_size.HasMember("width")) {
                log("block_size has no member \"width\"");
                return;
            }
            if (!block_size.HasMember("height")) {
                log("block_size has no member \"height\"");
                return;
            }

            if (!document["block_size"]["width"].IsInt()) {
                log("block_size/width wrong data type");
                return;
            }
            if (!document["block_size"]["height"].IsInt()) {
                log("block_size/height wrong data type");
                return;
            }

            int block_size_width_value = document["block_size"]["width"].GetInt();
            int block_size_height_value = document["block_size"]["height"].GetInt();
            log("block_size: %dx%d", block_size_width_value, block_size_height_value);

            if (!document.HasMember("game_speed")) {
                log("Document has no member \"game_speed\"");
                return;
            }

            if (!document["game_speed"].IsInt()) {
                log("game_speed wrong data type");
                return;
            }

            unsigned int game_speed_value = document["game_speed"].GetInt();
            log("Platform size: %d", game_speed_value);


            _board_size = Size(std::min(std::max(board_size_width_value, _min_board_size_width), _max_board_size_width),
                               std::min(std::max(board_size_height_value, _min_board_size_height), _max_board_size_height));
            _platform_size = std::min(std::max((int)platform_size_value, _min_platform_size), _max_platform_size);
            _block_size = Size(std::min(std::max(block_size_width_value, _min_block_size_width), _max_block_size_width),
                               std::min(std::max(block_size_height_value, _min_block_size_height), _max_block_size_height));
            _game_speed = std::min(std::max((int)game_speed_value, _min_game_speed), _max_game_speed);


            board_size_width->setString(std::to_string((int)_board_size.width));
            board_size_height->setString(std::to_string((int)_board_size.height));
            platform_size->setString(std::to_string(_platform_size));
            block_size_width->setString(std::to_string((int)_block_size.width));
            block_size_height->setString(std::to_string((int)_block_size.height));
            game_speed->setString(std::to_string((int)_game_speed));


        });
    load_settings_item->setPosition(Vec2(visibleSize.width / 2, 100));

    auto save_settings_item = MenuItemFont::create("Save settings", [&](Ref* sender) {
            rapidjson::Document document;

            document.SetObject();

            rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

            rapidjson::Value board_size(rapidjson::kObjectType);
            board_size.AddMember("width", (int)_board_size.width, allocator);
            board_size.AddMember("height", (int)_board_size.height, allocator);
            document.AddMember("board_size", board_size, allocator);

            document.AddMember("platform_size", (int)_platform_size, allocator);

            rapidjson::Value block_size(rapidjson::kObjectType);
            block_size.AddMember("width", (int)_block_size.width, allocator);
            block_size.AddMember("height", (int)_block_size.height, allocator);
            document.AddMember("block_size", block_size, allocator);

            document.AddMember("game_speed", (int)_game_speed, allocator);

            rapidjson::StringBuffer strbuf;
            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
            document.Accept(writer);

            std::string result_json(strbuf.GetString());
            log("JSON DATA:\n%s", result_json.c_str());

            std::ofstream file(FileUtils::getInstance()->fullPathForFilename("settings.json"));

            if (!file.is_open()) {
                log("Error opening file!");
                return;
            }

            file << result_json;

            file.close();


        });
    save_settings_item->setPosition(Vec2(visibleSize.width / 2, 60));

    auto menu = Menu::create(board_size_width_plus, board_size_width_minus,
                             board_size_height_plus, board_size_height_minus,
                             platform_size_plus, platform_size_minus,
                             block_size_width_plus, block_size_width_minus,
                             block_size_height_plus, block_size_height_minus,
                             game_speed_plus, game_speed_minus,
                             back_item,
                             play_item,
                             load_settings_item,
                             save_settings_item,
                             NULL);
    menu->setPosition(Vec2::ZERO);
    //menu->setPosition(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y);
    this->addChild(menu);

    return true;
}













