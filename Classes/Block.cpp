#include "Block.h"

namespace arcanoid
{

Block::Block() :
    _position(0.0f, 0.0f),
    _size(0),
    _solution("")
{

}

Block::Block(cocos2d::Point position, cocos2d::Size size) :
    _position(position),
    _size(size),
    _solution("")
{

}

Block::Block(cocos2d::Point position, cocos2d::Size size, std::string solution, bool correct) :
    _position(position),
    _size(size),
    _solution(solution),
    _correct(correct)
{

}

Block::~Block()
{

}

Block *Block::create()
{
    Block *block = new Block();
    if (block) {
        return block;
    }
    CC_SAFE_DELETE(block);
    return nullptr;
}

Block *Block::create(cocos2d::Point position, cocos2d::Size size)
{
    Block *block = new Block(position, size);
    if (block) {
        return block;
    }
    CC_SAFE_DELETE(block);
    return nullptr;
}

Block *Block::create(cocos2d::Point position, cocos2d::Size size, std::string solution, bool correct)
{
    Block *block = new Block(position, size, solution, correct);
    if (block) {
        return block;
    }
    CC_SAFE_DELETE(block);
    return nullptr;
}

cocos2d::Point Block::position() const
{
    return _position;
}

void Block::setPosition(const cocos2d::Point &position)
{
    _position = position;
}

cocos2d::Size Block::size() const
{
    return _size;
}

std::string Block::solution() const
{
    return _solution;
}

bool Block::correct() const
{
    return _correct;
}

}
