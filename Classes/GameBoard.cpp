#include "GameBoard.h"

namespace arcanoid
{

GameBoard::GameBoard() :
    _size_in_cells(cocos2d::Size(0.0f, 0.0f)),
    _cell_size_in_pixels(cocos2d::Size(0.0f, 0.0f)),
    _space_between_cells(0.0f),
    _cells_color(cocos2d::Color4F::WHITE)
{

}

GameBoard::GameBoard(cocos2d::Size board_size,
                     cocos2d::Size cell_size_in_pixels,
                     float space_between_cells,
                     cocos2d::Color4F cells_color) :
    _size_in_cells(board_size),
    _cell_size_in_pixels(cell_size_in_pixels),
    _space_between_cells(space_between_cells),
    _cells_color(cells_color)
{

}

GameBoard::GameBoard(const GameBoard &oth) :
    _size_in_cells(oth.size_in_cells()),
    _cell_size_in_pixels(oth.cell_size_in_pixels()),
    _cells_color(oth.cells_color())
{

}

GameBoard::~GameBoard()
{

}

GameBoard *GameBoard::create(cocos2d::Size board_size,
                             cocos2d::Size cell_size_in_pixels,
                             float space_between_cells,
                             cocos2d::Color4F cells_color)
{
    GameBoard *game_board = new GameBoard(board_size, cell_size_in_pixels, space_between_cells, cells_color);
    if (game_board) {
        return game_board;
    }
    CC_SAFE_DELETE(game_board);
    return nullptr;
}

GameBoard *GameBoard::create(const GameBoard &oth)
{
    GameBoard *game_board = new GameBoard(oth);
    if (game_board) {
        return game_board;
    }
    CC_SAFE_DELETE(game_board);
    return nullptr;
}

cocos2d::DrawNode *GameBoard::getDrawNodeForModel()
{
    auto Node = cocos2d::DrawNode::create(/*0.5f*/);

    for (size_t row = 0; row < _size_in_cells.width; ++row) {
        for (size_t col = 0; col < _size_in_cells.height; ++col) {
            auto origin = cocos2d::Vec2(row * (_cell_size_in_pixels.width + 1.0f),
                                        col * (_cell_size_in_pixels.height + 1.0f));
            Node->drawSolidRect(origin, origin + _cell_size_in_pixels, _cells_color);
        }
    }

    return Node;
}

cocos2d::Size GameBoard::size_in_cells() const
{
    return _size_in_cells;
}

cocos2d::Size GameBoard::cell_size_in_pixels() const
{
    return _cell_size_in_pixels;
}

cocos2d::Color4F GameBoard::cells_color() const
{
    return _cells_color;
}

cocos2d::Vec2 GameBoard::get_cell_coordinates(float x, float y)
{
    return cocos2d::Vec2(x * (_cell_size_in_pixels.width + _space_between_cells),
                         y * (_cell_size_in_pixels.height + _space_between_cells));
}

cocos2d::Vec2 GameBoard::get_cell_coordinates(cocos2d::Vec2 cell)
{
    return cocos2d::Vec2(cell.x * (_cell_size_in_pixels.width + _space_between_cells),
                         cell.y * (_cell_size_in_pixels.height + _space_between_cells));
}

}
