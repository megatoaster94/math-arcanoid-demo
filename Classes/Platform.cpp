#include "Platform.h"

#include "cocos2d.h"

namespace arcanoid
{

Platform::Platform() :
    _size(0),
    _expression(""),
    _position(0)
{

}

Platform::Platform(size_t size) :
    _size(size),
    _expression(""),
    _position(0)
{

}

Platform::Platform(size_t size, std::string expression) :
    _size(size),
    _expression(expression),
    _position(0)
{

}

Platform::Platform(size_t size, std::string expression, unsigned int position) :
    _size(size),
    _expression(expression),
    _position(position)
{

}

Platform::~Platform()
{

}

Platform *Platform::create()
{
    Platform *platform = new Platform();
    if (platform) {
        return platform;
    }
    CC_SAFE_DELETE(platform);
    return nullptr;
}

Platform *Platform::create(size_t size)
{
    Platform *platform = new Platform(size);
    if (platform) {
        return platform;
    }
    CC_SAFE_DELETE(platform);
    return nullptr;
}

Platform *Platform::create(size_t size, std::string expression)
{
    Platform *platform = new Platform(size, expression);
    if (platform) {
        return platform;
    }
    CC_SAFE_DELETE(platform);
    return nullptr;
}

Platform *Platform::create(size_t size, std::string expression, unsigned int position)
{
    Platform *platform = new Platform(size, expression, position);
    if (platform) {
        return platform;
    }
    CC_SAFE_DELETE(platform);
    return nullptr;
}

size_t Platform::size() const
{
    return _size;
}

std::string Platform::expression() const
{
    return _expression;
}

void Platform::setExpression(const std::string &expression)
{
    _expression = expression;
}

unsigned int Platform::position() const
{
    return _position;
}

void Platform::setPosition(const unsigned int &position)
{
    _position = position;
}

}
