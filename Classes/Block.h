#ifndef __BLOCK_H__
#define __BLOCK_H__

#include "cocos2d.h"

namespace arcanoid {

class Block
{
public:
    Block();
    Block(cocos2d::Point position, cocos2d::Size size);
    Block(cocos2d::Point position, cocos2d::Size size, std::string solution, bool correct);
    virtual ~Block();

    static Block* create();
    static Block* create(cocos2d::Point position, cocos2d::Size size);
    static Block* create(cocos2d::Point position, cocos2d::Size size, std::string solution, bool correct);

    friend void swap(Block& lhs, Block& rhs)
    {
        std::swap(lhs._position, rhs._position);
        std::swap(lhs._size, rhs._size);
        std::swap(lhs._solution, rhs._solution);
        std::swap(lhs._correct, rhs._correct);
    }

    cocos2d::Point position() const;
    void setPosition(const cocos2d::Point &position);

    cocos2d::Size size() const;

    std::string solution() const;

    bool correct() const;

private:
    //Block &operator =(const Block &) = delete;

    cocos2d::Point _position;
    cocos2d::Size _size;
    std::string _solution;
    bool _correct;

};

}

#endif
