#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <string>

namespace arcanoid {

class Platform
{
public:
    Platform();
    Platform(size_t size);
    Platform(size_t size, std::string expression);
    Platform(size_t size, std::string expression, unsigned int position);
    virtual ~Platform();

    static Platform* create();
    static Platform* create(size_t size);
    static Platform* create(size_t size, std::string expression);
    static Platform* create(size_t size, std::string expression, unsigned int position);

    size_t size() const;
    std::string expression() const;
    void setExpression(const std::string &expression);
    unsigned int position() const;
    void setPosition(const unsigned int &position);

private:
    Platform &operator =(const Platform &) = delete;

    size_t _size;
    std::string _expression;
    unsigned int _position;
};

}

#endif
