#pragma once

#include <string>
#include <vector>

class StringHelper {
public:
    // split string with string delimiter
    static std::vector<std::string> split(std::string str, std::string delimiter);

    // trim from start (in place)
    static void ltrim(std::string &s);

    // trim from end (in place)
    static void rtrim(std::string &s);

    // trim from both ends (in place)
    static void vtrim(std::string &s);

    // trim from start (copying)
    static std::string ltrim_copy(std::string s);

    // trim from end (copying)
    static std::string rtrim_copy(std::string s);

    // trim from both ends (copying)
    static std::string trim_copy(std::string s);
    
    // trim from both ends & inside the string(copying)
    static std::string trim(std::string txt, const char ch);

    // split string with char delimiter
    static std::vector<std::string> split(std::string str, const char ch);
    
    // join a string of vector elements
    static std::string join(std::vector<std::string> vec, const char ch);

    // replace from string on to string in str
    static bool replace(std::string &str, const std::string &from, const std::string &to);

    // replace vector[old string] on vector[new string] in str
    static bool replace_all(std::string &str, const std::vector<std::string> &old, const std::vector<std::string> &nw);

    static bool is_numeric(std::string str);
};
