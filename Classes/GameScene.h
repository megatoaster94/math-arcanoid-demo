#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"

#include "GameBoard.h"
#include "Platform.h"
#include "Block.h"

#include <memory>

class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    static cocos2d::Scene* createScene(cocos2d::Size board_size,
                                       unsigned int platform_size,
                                       cocos2d::Size block_size,
                                       float game_speed,
                                       std::queue<std::string> expressions,
                                       std::queue<std::queue<arcanoid::Block>> blocks);

    virtual bool init();

    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

    void blockSpawner(float);
    void blockMover(float);
    void blockCollisionChecker(float);

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
private:
    //TODO: make Round X label ?
    unsigned int _round = 1;
    float _game_speed;

    std::unique_ptr<arcanoid::GameBoard> _game_board_model;
    cocos2d::DrawNode *_game_board_node;
    cocos2d::Label *_platform_label;

    std::unique_ptr<arcanoid::Platform> _platform_model;
    cocos2d::DrawNode *_platform_node;

    std::queue<std::string> _expressions;
    std::queue<std::queue<arcanoid::Block>> _blocks_queue;

    std::deque<arcanoid::Block> _blocks_models_on_board;
    std::deque<cocos2d::DrawNode*> _blocks_nodes_on_board;

};

#endif // __GAME_SCENE_H__
