#ifndef __GAME_BOARD_H__
#define __GAME_BOARD_H__

#include "cocos2d.h"

namespace arcanoid {

class GameBoard
{
public:
    GameBoard();
    GameBoard(cocos2d::Size board_size,
              cocos2d::Size cell_size_in_pixels,
              float space_between_cells,
              cocos2d::Color4F cells_color);
    GameBoard(const GameBoard &oth);
    virtual ~GameBoard();

    static GameBoard* create(cocos2d::Size board_size,
                             cocos2d::Size cell_size_in_pixels,
                             float space_between_cells = 1.0f,
                             cocos2d::Color4F cells_color = cocos2d::Color4F::WHITE);
    static GameBoard* create(const GameBoard &oth);

    cocos2d::DrawNode *getDrawNodeForModel();

    cocos2d::Size size_in_cells() const;

    cocos2d::Size cell_size_in_pixels() const;

    cocos2d::Color4F cells_color() const;

    cocos2d::Vec2 get_cell_coordinates(float x, float y);
    cocos2d::Vec2 get_cell_coordinates(cocos2d::Vec2 cell);

private:
    GameBoard &operator =(const GameBoard &) = delete;

    cocos2d::Size _size_in_cells;
    cocos2d::Size _cell_size_in_pixels;
    float _space_between_cells;

    cocos2d::Color4F _cells_color;
};

}

#endif
