/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __GAMESETTINGS_SCENE_H__
#define __GAMESETTINGS_SCENE_H__

#include "cocos2d.h"

class GameSettingsScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameSettingsScene);
private:
    cocos2d::Size _board_size{20, 20};
    unsigned int _platform_size = 4;
    cocos2d::Size _block_size{2, 2};
    float _game_speed = 1;

    const int _min_board_size_width = 10;
    const int _max_board_size_width = 40;
    const int _min_board_size_height = 10;
    const int _max_board_size_height = 25;
    const int _min_platform_size = 1;
    const int _max_platform_size = 5;
    const int _min_block_size_width = 1;
    const int _max_block_size_width = 5;
    const int _min_block_size_height = 1;
    const int _max_block_size_height = 5;
    const int _min_game_speed = 1;
    const int _max_game_speed = 10;

};

#endif // __GAMESETTINGS_SCENE_H__
