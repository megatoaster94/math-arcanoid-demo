#include "GameScene.h"

USING_NS_CC;

#define BLOCK_TAG 0x666

Size board_sizeGS;
unsigned int platform_sizeGS;
Size block_sizeGS;
float game_speedGS;
std::queue<std::string> expressionsGS;
std::queue<std::queue<arcanoid::Block>> blocksGS;

Scene* GameScene::createScene()
{
    return GameScene::create();
}

Scene* GameScene::createScene(Size board_size, unsigned int platform_size, Size block_size, float game_speed, std::queue<std::string> expressions, std::queue<std::queue<arcanoid::Block> > blocks)
{
    board_sizeGS = board_size;
    platform_sizeGS = platform_size;
    block_sizeGS = block_size;
    game_speedGS = game_speed;
    expressionsGS = expressions;
    blocksGS = blocks;

    return GameScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto label = Label::createWithTTF("GAME", "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }

    _expressions = expressionsGS;
    _blocks_queue = blocksGS;

    //_game_board_model = std::make_shared<arcanoid::GameBoard>(Size(20.0f, 20.0f), Size(20.0f, 20.0f), 1.0f, Color4F::WHITE);
    //_game_board_model.reset(arcanoid::GameBoard::create(Size(20.0f, 20.0f), Size(20.0f, 20.0f)));
    _game_board_model.reset(arcanoid::GameBoard::create(board_sizeGS, Size(20.0f, 20.0f)));

    auto game_board_size = _game_board_model->size_in_cells();
    auto game_board_cell_size = _game_board_model->cell_size_in_pixels();

    _game_board_node = _game_board_model->getDrawNodeForModel();

    _game_board_node->setPosition(Vec2(visibleSize.width/2 - (game_board_size.width * (game_board_cell_size.width + 1.0f))/2,
                                       visibleSize.height/2 - (game_board_size.height * (game_board_cell_size.height + 1.0f))/2));

    this->addChild(_game_board_node);


    //_platform_model = std::make_shared<arcanoid::Platform>(3, "test");
    _platform_model.reset(arcanoid::Platform::create(platform_sizeGS, "test"));

    auto platform_size = _platform_model->size();

    _platform_node = DrawNode::create();

    for (size_t i = 0; i < platform_size; ++i) {
        auto origin = Vec2(i * (game_board_cell_size.width + 1.0f), 0);
        _platform_node->drawSolidRect(origin, origin + game_board_cell_size, Color4F::RED);
    }

    _platform_label = Label::createWithSystemFont(_expressions.front(), "Arial", 18);
    _platform_label->setPosition(platform_size * game_board_cell_size.width / 2, game_board_cell_size.height / 2);
    _platform_node->addChild(_platform_label);

    auto coordinates = _game_board_model->get_cell_coordinates(_platform_model->position(), 0);
    _platform_node->setPosition(coordinates);

    _game_board_node->addChild(_platform_node);


    _game_speed = game_speedGS;


    auto keyboard_listener = EventListenerKeyboard::create();
    keyboard_listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
    keyboard_listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(keyboard_listener, this);

    this->schedule(CC_SCHEDULE_SELECTOR(GameScene::blockSpawner));
    this->schedule(CC_SCHEDULE_SELECTOR(GameScene::blockMover));
    this->schedule(CC_SCHEDULE_SELECTOR(GameScene::blockCollisionChecker));

    return true;
}


void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
    switch(keyCode){
    case EventKeyboard::KeyCode::KEY_ESCAPE:
        Director::getInstance()->popScene();
        break;
    case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
    case EventKeyboard::KeyCode::KEY_A:
    {
        if (_platform_model->position() > 0) {
            _platform_model->setPosition(_platform_model->position() - 1);
            auto coordinates = _game_board_model->get_cell_coordinates(_platform_model->position(), 0);
            _platform_node->setPosition(coordinates);
        }
        break;
    }
    case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
    case EventKeyboard::KeyCode::KEY_D:
    {
        if (_platform_model->position() < _game_board_model->size_in_cells().width - _platform_model->size()) {
            _platform_model->setPosition(_platform_model->position() + 1);
            auto coordinates = _game_board_model->get_cell_coordinates(_platform_model->position(), 0);
            _platform_node->setPosition(coordinates);
        }
        break;
    }
    case EventKeyboard::KeyCode::KEY_UP_ARROW:
    case EventKeyboard::KeyCode::KEY_W:
        break;
    case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
    case EventKeyboard::KeyCode::KEY_S:
        break;
    case EventKeyboard::KeyCode::KEY_SPACE:
        break;
    }
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{

}

void GameScene::blockSpawner(float dt)
{
    static float time_passed = 0;

    time_passed += dt;

    if (_blocks_queue.front().empty() || time_passed < 3.0f / _game_speed) {
        return;
    }

    time_passed = 0;

    arcanoid::Block &block_model = _blocks_queue.front().front();

    //TODO: make getDrawNodeFromModel method
    auto block_node = DrawNode::create();

    auto block_size = block_model.size();
    block_model.setPosition(Vec2(RandomHelper::random_int(0, (int)(_game_board_model->size_in_cells().width - block_size.width)),
                                 _game_board_model->size_in_cells().height - block_size.height));
    auto block_pos = block_model.position();

    for (size_t i = 0; i < block_size.width; ++i) {
        for (size_t j = 0; j < block_size.height; ++j) {
            auto origin = Vec2(i * (_game_board_model->cell_size_in_pixels().width + 1.0f),
                               j * (_game_board_model->cell_size_in_pixels().height + 1.0f));
            block_node->drawSolidRect(origin, origin + _game_board_model->cell_size_in_pixels(), Color4F::BLUE);
        }
    }

    auto block_label = Label::createWithSystemFont(block_model.solution(), "Arial", 20);
    block_label->setPosition(block_size.width * _game_board_model->size_in_cells().width / 2,
                             block_size.height * _game_board_model->size_in_cells().height / 2);
    block_node->addChild(block_label);

    auto coordinates = _game_board_model->get_cell_coordinates(block_pos);
    block_node->setPosition(coordinates);

    _game_board_node->addChild(block_node, 0, "block");
    _blocks_models_on_board.push_back(block_model);
    _blocks_nodes_on_board.push_back(block_node);
    _blocks_queue.front().pop();
}

void GameScene::blockMover(float dt)
{
    static float time_passed = 0;

    time_passed += dt;

    //log("Time passed: %f", time_passed);

    if (time_passed < 1.0f / _game_speed) {
        return;
    }

    time_passed = 0;

    if (_blocks_nodes_on_board.empty()) {
        return;
    }

    for (size_t i = 0; i < _blocks_nodes_on_board.size(); ++i) {
        _blocks_models_on_board[i].setPosition(_blocks_models_on_board[i].position() - Vec2(0, 1));
        _blocks_nodes_on_board[i]->setPosition(_game_board_model->get_cell_coordinates(_blocks_models_on_board[i].position()));
    }

    /*arcanoid::Block &cur_block_model = _blocks_queue.front().front();
    auto cur_block_node = _game_board_node->getChildByName("block");

    cur_block_model.setPosition(cur_block_model.position() - Vec2(0, 1));

    cur_block_node->setPosition(_game_board_model->get_cell_coordinates(cur_block_model.position()));*/

}

void GameScene::blockCollisionChecker(float dt)
{
    if (_blocks_nodes_on_board.empty()) {
        return;
    }

    for (size_t i = 0; i < _blocks_models_on_board.size(); ++i) {
        //arcanoid::Block &block_model = _blocks_queue.front().front();

        auto block_pos = _blocks_models_on_board[i].position();
        auto block_size = _blocks_models_on_board[i].size();

        auto platform_pos = _platform_model->position();
        auto platform_size = _platform_model->size();

        for (size_t block_row = block_pos.x; block_row < block_pos.x + block_size.width; ++block_row) {
            for (size_t platform_row = platform_pos; platform_row < platform_pos + platform_size; ++platform_row) {
                if (_game_board_model->get_cell_coordinates(block_row, block_pos.y).equals(
                            _game_board_model->get_cell_coordinates(platform_row, 0))) {
                    if (_blocks_models_on_board[i].correct()) {
                        _expressions.pop();
                        _blocks_queue.pop();
                        for (auto node : _blocks_nodes_on_board) {
                            _game_board_node->removeChild(node);
                        }
                        _blocks_models_on_board.clear();
                        _blocks_nodes_on_board.clear();
                        if (_blocks_queue.empty()) {
                            log("You win!");
                            Director::getInstance()->popScene();
                        }
                        else {
                            _platform_label->setString(_expressions.front());
                            _round++;
                            _game_speed += 1.0f;
                        }
                    }
                    else {
                        log("You lose. Wrong block!");
                        Director::getInstance()->popScene();
                    }
                    return;
                }
            }
        }

        if (block_pos.y == 0) {
            /*if (_blocks_models_on_board[i].correct()) {
                log("You lose!");
                Director::getInstance()->popScene();
            }*/
            _game_board_node->removeChild(_blocks_nodes_on_board[i]);
            _blocks_queue.front().push(_blocks_models_on_board[i]);
            _blocks_nodes_on_board.erase(_blocks_nodes_on_board.begin() + i);
            _blocks_models_on_board.erase(_blocks_models_on_board.begin() + i);
        }
    }

}


void GameScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    //Director::getInstance()->end();
    Director::getInstance()->popScene();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}









